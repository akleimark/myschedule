package client;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ComponentListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import view.View;
import controller.Controller;
import exception.ExitException;
/**
 * MySchedule is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MySchedule is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MySchedule. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Klassen GUI utg�r sj�lva k�rnan i det grafiska gr�nssnittet.
 * Klassen �rver klassen JFrame fr�n Swing. Klassen implementerar dessutom gr�nssnittet "View", 
 * f�r att kunna uppdateras, d� f�r�ndringar intr�ffar. I detta fallet handlar det om f�nsterstorleken. 
 * Om anv�ndaren �ndrar p� den kommer huvudf�nstret att ritas om och komponeneterna skalas om. 
 * 
 * @Utvecklare: Anders Kleimark
 *
 */

@SuppressWarnings("serial")
public class GUI extends JFrame implements View
{
	private static final int MIN_WIDTH = 200; // Minsta bredd p� f�nstret �r 200 pixlar.
	private static final int MIN_HEIGHT = 200;// // Minsta h�jd p� f�nstret �r 200 pixlar.
	private static int width; // Den egentliga bredden p� applikationsf�nstret.
	private static int height; // Den egentliga h�jden p� applikationsf�nstret.
	
	/**
	 * Den f�rvalda konstruktorn skapar ett f�nster, vars h�jd och bredd utg�r v�rdet p� 
	 * den minsta bredd, respektive h�jd. Konstruktorn initierar ocks� de ovan beskrivna datamedlemmarna, 
	 * samt visar f�nstret.  
	 * 
	 * @throws ExitException
	 */
	
	public GUI() throws ExitException 
	{
		GUI.width =  GUI.MIN_WIDTH;
		GUI.height = GUI.MIN_HEIGHT; 
		initialize();		
		setUp();			
	} 
	
	/**
	 * En konstruktor med tv� parametrar f�r bredd, respektive h�jd p� f�nsterstorleken. 
	 * @param w
	 * @param h
	 * @throws ExitException
	 */
	public GUI(int w, int h) throws ExitException 
	{
		GUI.width =  Math.max(GUI.MIN_WIDTH, w);
		GUI.height =  Math.max(GUI.MIN_HEIGHT, h);
				
		initialize();
		setUp();
		
	} 
	
	/**
	 * Initierar f�nstrets basv�rden. 
	 */
	private void initialize()
	{
		setTitle("MySchedule");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setPreferredSize(new Dimension(GUI.width, GUI.height));
		setLayout(new FlowLayout());				
	}

	
	/**
	 * Vi slutf�r initieringen av f�nstret (frame).
	 */
	private void setUp()
	{
		pack();
		setVisible(true);
	}
	
	/**
	 * 
	 * @return: h�jden p� f�nstret.
	 */
	public static int getFrameHeight()
	{
		return GUI.height;
	}
	
	/**
	 * 
	 * @return: bredden p� f�nstret.
	 */
	public static int getFrameWidth()
	{
		return GUI.width;
	}
	
	/**
	 * 
	 * @param h: s�tter f�nsterh�jden. 
	 */
	public static void setHeight(int h)
	{
		height = Math.max(MIN_HEIGHT, h);
	}
	
	
	/**
	 * 
	 * @param w: s�tter f�nsterbredden. 
	 */
	public static void setWidth(int w)
	{
		width = Math.max(MIN_WIDTH, w);
	}

	/**
	 * 
	 * @return: en referens till f�nstret. 
	 */
	public JFrame getFrame()
	{
		return this;
	}

	/**
	 * Varje instans av klassen GUI �r en vy, d.v.s en statisk sida, vars uppgift endast �r  
	 * att visa en sida f�r anv�ndaren. MVC har �ven metoder, som har hand om all data. Metoderna 
	 * kan sedan ha en eller flera vyer. Kontrollerarna � andra sidan �r de som anv�nder metoderna f�r att 
	 * uppdatera vyerna. Denna uppdatering sker d� i den h�r metoden. I det h�r fallet uppdateras huvudf�nstrets 
	 * h�jd, respektive bredd f�r att vyn skall visas p� ett korrekt vis. 
	 */
	@Override
	public void update(Object object)
	{				
		
		int[] widthHeight = (int[]) object;
	
		GUI.setWidth(widthHeight[0]);
		GUI.setHeight(widthHeight[1]);
		
		repaint();
		
		//JOptionPane.showMessageDialog(null, "GUI:" + GUI.getFrameWidth());
		
	}

	@Override
	public void render()
	{
		
		
	}

	/**
	 * Vi l�gger till en kontrollerare till den h�r vyn (GUI). 
	 */
	@Override
	public void addController(Controller controller)
	{
		addComponentListener((ComponentListener) controller);
		
	}

	
	@Override
	public String getViewType()
	{
		return "GUI";
	}
	
	
}
