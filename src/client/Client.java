package client;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JPanel;
import model.ApplicationDataModel;
import model.ViewModel;
import view.View;
import controller.FrameSizeController;
import controller.HomeController;
import exception.DatabaseException;
import exception.ExitException;
import extra.JLabelWait;
import factory.ButtonPanelViewFactory;
import factory.ControllerFactory;
import factory.FrameSizeControllerFactory;
import factory.HomeControllerFactory;
import factory.MainViewFactory;
import factory.ViewFactory;

/**
 * MySchedule is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MySchedule is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MySchedule. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * MySchedule �r ett schemaprogram, som anv�nds om man �r i behov av ett veckoschema f�r en skolklass. 
 * Lektionerna skapas via ett enkelt gr�nssnitt, d�r anv�ndaren matar in starttiden, sluttiden och andra n�dv�ndiga data. 
 * De lektioner/raster/matraster som har lagts till kan anv�ndaren enkelt f� en �verblick �ver. Det g�r �ven att radera 
 * befintliga data. Med hj�lp av menyalternativet "Visa schemat" f�r anv�ndaren se det skapade schemat. Slutligen 
 * g�r det �ven att skapa olika dokument f�r schemat. De format som det finns st�d f�r �r ett vanligt textdokument, 
 * en schemabild och ett pdf-dokument. Om ett vanligt textdokument genereras visas de inlagda 
 * lektionerna/rasterna/matrasterna i vanlig text. Genereras i st�llet en bild skapas en bild i pgn-formatet. 
 * En pdf kan ocks� genereras. Det �r f�rmodligen det formatet som b�st l�mpar sig d� schemat ska skrivas ut eller 
 * skickas till andra. I alla fall d� dokument skapas f�r anv�ndaren sj�lv v�lja vilken m�lfil som ska skapas. 
 * 
 * Programmet har �ven st�d f�r egna s� kallade "lektionsbilder". Detta inneb�r att det g�r att l�gga 
 * in egna bilder f�r att symbolisera en specifik lektion. I katalogen "images" kan anv�ndaren d�r sj�lv 
 * l�gga in sina bilder. Vill anv�ndaren d� exempelvis skapa en lektionsbild f�r matematik skapas en bild 
 * "Matematik.jpg" i katalogen "images". Denna bilden kommer sedan att visas vid de lektionstillf�llen 
 * som matematik f�rekommer. N�r egna bilder l�ggs in i den katalogen �r det bra om dessa bilder 
 * inte �r f�r stora, d� det tenderar att g�ra textutrymmet i lektionstillf�llena litet. Bilden kan 
 * dessutom inte vara st�rre �n h�jd/bredd p� lektionstillf�llet. Det finns inget st�d f�r skalning av de 
 * inlagda bilderna, utan anv�ndaren ansvarar sj�lv f�r att bilderna �r tillr�ckligt stora/sm�. 
 * 
 * MySchedule anv�nder arkitekturmodellen Model View Controller (MVC). MVC skiljer p� modeller, vyer och kontrollerare. 
 * Kontrollerarna �r de som sk�ter sj�lva kontrollen av applikationen. De skickar data till vyerna och 
 * uppdaterar modellerna. Vyerna �r i st�llet de som visar inneh�ll f�r anv�ndaren. Modellerna inneh�ller data.
 * 
 * I de fall d�r data ska sparas mellan sessionerna beh�vs n�gon form av databashanterare. MySchedule anv�nder
 * databashanteraren "OpenJPA". OpenJPA �r licenserat under "Apache licence version 2". En kopia av denna licensen 
 * finns i katalogen "licenses". 
 * 
 * Vidare anv�nder MySchedule tredjepartsbiblioteket "iText", f�r att skapa ett pdf-dokumet av schemat. 
 * iText �r i st�llet licenserat under "Affero General Public License version 3". 
 * 
 * MySchedule anv�nder i sin tur "GNU General Public License version 3".
 * 
 * Alla licenser finns i katalogen "licenses".
 * 
 * Slutligen kr�vs applikationen "Ant" f�r att kunna bygga programmet med byggskriptet "build.xml". 
 * 
 * Bor�s 2013-10-14
 * 
 *
 * @Utvecklare: Anders Kleimark
 *
 */

public class Client implements Runnable
{	
	private GUI gui; // Huvudf�nstret.
	private ControllerFactory controllerFactory; // F�r att kunna skapa kontrollerare.
	private ViewFactory viewFactory; // F�r att kunna skapa vyer.
	private View mainView; // Huvudvyn.
	private View buttonPanel; // Menyn.
	private HomeController homeController; // En "HomeController".
	private FrameSizeController frameSizeController; // En "GraphicController".
	
	public Client() throws ExitException, DatabaseException
	{				
		controllerFactory = null;
		viewFactory = null;
		initialize();
		initializeMVC();
		
	}

	/**
	 * Initierar de vyer som applikationen anv�nder sig av. 
	 * @throws ExitException
	 */
	private void initialize() throws ExitException
	{
		gui = new GUI(1000, 800);
		
		// Vi skapar menyn.
		
		viewFactory = new ButtonPanelViewFactory();
		buttonPanel = viewFactory.createProduct();
		buttonPanel.render();
		
		// Vi l�ter menyn utg�ra 1/20 av hela f�nstret. 
		((Component) buttonPanel).setPreferredSize(new Dimension(GUI.getFrameWidth(), GUI.getFrameHeight()/20));
		gui.add((Component) buttonPanel);
		
		viewFactory = new MainViewFactory();
		((MainViewFactory) viewFactory).setFrame(gui); //Utrymmet som anv�nds f�r applikationen (exklusive menyn).		
		mainView = viewFactory.createProduct();
		mainView.render(); //Menyn skrivs ut.
		gui.add((Component) mainView); 
		
		
	}
	
	/**
	 * Kopplar vyerna till de skapade kontrollerarna. En "ApplicationDataModel" skapas 
	 * ocks�, f�r att avg�ra och agera om huvudf�nstret �ndrar storlek. 
	 * @throws DatabaseException 
	 */
	private synchronized void initializeMVC() throws ExitException, DatabaseException
	{
		JLabelWait label = new JLabelWait();
		label.setFont(new Font(Font.SERIF, Font.BOLD, 22));
		
		Thread thread = new Thread(this);
		thread.start();
		try
		{
			((JPanel) mainView).add(label);
			wait();
		}
		catch (InterruptedException e)
		{
			throw new ExitException();
		}
		
		label.setVisible(false);
		ViewModel model = new ViewModel();
					
		homeController.setModel(model);
		model.add(mainView);
		buttonPanel.addController(homeController);
		
		ApplicationDataModel applicationDataModel = new ApplicationDataModel();
		applicationDataModel.add(gui);				
		frameSizeController.setModel(applicationDataModel);
		gui.addController(frameSizeController); // Vi l�gger till lyssnaren.
		
		
				
	}
			
	
	/**
	 * Main-metoden skapar en instans av klassen Client.
	 * @param args
	 * @throws ExitException
	 * @throws DatabaseException 
	 */
	public static void main(String[] args) throws ExitException, DatabaseException
	{
		new Client();
						
	}

	/**
	 * Synkroniserad metod f�r att skapa v�ra kontrollerare. 
	 */
	@Override
	public synchronized void run()
	{
		
		try
		{
			controllerFactory = new HomeControllerFactory();		
			homeController = (HomeController) controllerFactory.createProduct();
			
			controllerFactory = new FrameSizeControllerFactory();		
			frameSizeController = (FrameSizeController) controllerFactory.createProduct();
		}
		catch (DatabaseException e)
		{
			new ExitException();			
		} 
						
		notify();		
	}
	
	
}
