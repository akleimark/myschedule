package model;

/**
 * MySchedule is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MySchedule is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MySchedule. If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.Serializable;
import java.util.Iterator;
import java.util.Vector;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import view.View;

@SuppressWarnings("serial")
@Entity
@Table(name = "IMAGE")
public class Image extends Model implements Serializable 
{
	@Id 
    @Column(name = "IMAGE_ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long imageID;	
	
	@Column(name = "SOURCE", length = 128, nullable = false)
	private String source;
	
	@OneToOne(optional=false, cascade = { CascadeType.PERSIST })
    private SchoolSubject schoolSubject;  
	
	private Vector<View> viewList;
	private String [] sendingObject;
	
	public Image()
	{
		viewList = new Vector<View>();
		sendingObject = new String[2];
	}
	
	public Image(String p_source)
	{
		viewList = new Vector<View>();
		sendingObject = new String[2];
		source = p_source;
	}
	
	public void setSchoolSubject(SchoolSubject p_schoolSubject)
	{
		schoolSubject = p_schoolSubject;
		sendingObject[0] = schoolSubject.getName();
	}

	public SchoolSubject getSchoolSubject()
	{
		return schoolSubject;
	}
	
	public void setImageSource(String p_source)
	{
		source = p_source;
		sendingObject[1] = source;
	}
	
	
	public String getImageSource()
	{
		return source;
	}
		

	@Override
	public void add(View view)
	{
		viewList.add(view);
		
	}

	@Override
	public void remove(View view)
	{
		viewList.remove(view);
		
	}

	@Override
	public void notify(View view)
	{										
		
		view.update(sendingObject);
		
	}

	@Override
	public void notifyAllObservers()
	{
		Iterator<View> it = viewList.iterator();
		
		while(it.hasNext())
		{
			it.next().update(sendingObject);
		}
		
	}

	
	
	
}
