package model;

/**
 * MySchedule is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MySchedule is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MySchedule. If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.Iterator;
import java.util.Vector;
import view.View;

public class ApplicationDataModel extends Model
{
	private static int [] data;
	private static Vector<View> viewList;

	public ApplicationDataModel()
	{
		if(ApplicationDataModel.viewList == null)
		{
			viewList = new Vector<View>();
			ApplicationDataModel.data = new int[2];
		}
		
	}
	
	@Override
	public void add(View view)
	{
		ApplicationDataModel.viewList.add(view);
		
	}

	@Override
	public void remove(View view)
	{
		ApplicationDataModel.viewList.remove(view);
		
	}

	@Override
	public void notify(View view)
	{
		
		view.update(data);
		
	}

	@Override
	public void notifyAllObservers()
	{
		Iterator<View> it = ApplicationDataModel.viewList.iterator();
		
		while(it.hasNext())
		{
			it.next().update(data);
		}
		
	}
	
	public void setFrameDimension(int width, int height)
	{
		ApplicationDataModel.data = new int[2];
		ApplicationDataModel.data[0] = width;
		ApplicationDataModel.data[1] = height;
		notifyAllObservers();
	}
	
	
	
}
