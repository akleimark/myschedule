package model;

/**
 * MySchedule is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MySchedule is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MySchedule. If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import view.View;

/**
 * Klassen "Cource" handhar alla instanser av lektioner. En lektion 
 * kan skapas dels med hj�lp av den parameterl�sa konstruktorn, samt med hj�lp av 
 * dag, starttid och sluttid. 
 * 
 * @author anders
 *
 */


@SuppressWarnings("serial")
@Entity
@Table(name = "cource")
public class Cource extends Model implements Serializable
{
	
	@Id 
    @Column(name = "COURCE_ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long courceID;	
	
	@Column(name = "DAY", length = 32, nullable = false)
	private String day;
	
	@Column(name = "START_HOUR", nullable = false)
	private int startHour;
	
	@Column(name = "END_HOUR", nullable = false)
	private int endHour;
	
	@Column(name = "START_MIN", nullable = false)
	private int startMin;
	
	@Column(name = "END_MIN", nullable = false)
	private int endMin;
	
	@ManyToOne(optional = false, cascade = { CascadeType.PERSIST })
	SchoolSubject subject;
	
	
	/**
	 * F�rvald konstruktor, som skapar en instans av en lektion som startar klockan 08.00 
	 * p� m�ndagen och slutar klockan 09.00.
	 */
	public Cource()
	{
		day = "m�ndag";
		startHour = 8;
		endHour = 9;
		startMin = 0;
		endMin = 0;
		
		
	}
	/**
	 * En konstruktor som s�tter samma datamedlemmar som den parameterl�sa konstruktorn, men som 
	 * �ven s�tter skol�mnet. 
	 * @param p_subject
	 */
	public Cource(SchoolSubject p_subject)
	{
		day = "m�ndag";
		startHour = 8;
		endHour = 9;
		startMin = 0;
		endMin = 0;
		subject = p_subject;
		
	}
	
	/**
	 * En konstruktor som s�tter alla datamedlemmarna utom skol�mnet.
	 * @param p_day: veckodag f�r lektionen. 
	 * @param p_start_hour: starttimme
	 * @param p_start_min: startminut
	 * @param p_end_hour: sluttimme
	 * @param p_end_min: slutminut
	 */
	public Cource(String p_day, int p_start_hour, int p_start_min, int p_end_hour, int p_end_min) 
	{
		day = p_day;
		startHour = p_start_hour;
		endHour = p_end_hour;
		startMin = p_start_min;
		endMin = p_end_min;
	}
	/**
	 * 
	 * @return: starttimmen (int)
	 */
	public int getStartHour()
	{
		return startHour;
	}
	/**
	 * 
	 * @param hour: s�tter starttimmen.
	 */
	public void setStartHour(int hour)
	{
		startHour = hour;
	}
	
	/**
	 * 
	 * @return: startminuten (int).
	 */
	public int getStartMin()
	{
		return startMin;
	}
	
	/**
	 * 
	 * @param min: s�tter startminuten. 
	 */
	public void setStartMin(int min)
	{
		startMin = min;
	}
	
	/**
	 * 
	 * @return: sluttimmen (int)
	 */
	public int getEndHour()
	{
		return endHour;
	}
	
	/**
	 * 
	 * @param hour: s�tter sluttimmen.
	 */
	public void setEndHour(int hour)
	{
		endHour = hour;
	}
	
	/**
	 * 
	 * @return: slutminut (int).
	 */
	public int getEndMin()
	{
		return endMin;
	}
	
	/**
	 * 
	 * @param min: s�tter slutminuten.
	 */
	public void setEndMin(int min)
	{
		endMin = min;
	}
	
	/**
	 * S�tter skol�mnet (SchoolSUbject).
	 * @param p_subject
	 */
	public void setSubject(SchoolSubject p_subject)
	{
		subject = p_subject;
	}
	
	/**
	 * 
	 * @return: skol�mnet.
	 */
	public SchoolSubject getSubject()
	{
		return subject;
	}
	
	/**
	 * 
	 * @return: veckodag f�r lektionen.
	 */
	public String getDay()
	{
		return day;
	}

	/**
	 * En enkel utskrift.
	 */
	public void print()
	{
		System.out.println("�mne: " + subject.getName());
	}
	
	/**
	 * 
	 * @param cource: lektion
	 * @return: Avg�r huruvida en lektion startar tidigare �n en annan lektion. 
	 */
	public boolean courceStartedEarlier(Cource cource)
	{
		if(startHour * 60 + startMin < (cource.getStartHour() * 60 + cource.getStartMin()))
		{
			return true;
		}
		
		return false;
		
	}
	/**
	 * 
	 * @param cource: lektion
	 * @return: Avg�r huruvida en lektion slutar tidigare �n en annan lektion. 
	 */
	public boolean courceEndedEarlier(Cource cource)
	{
		if(endHour * 60 + endMin < (cource.getEndHour() * 60 + cource.getEndMin()))
		{
			return true;
		}
		
		return false;
		
	}
	
	/**
	 * Returnerar ett positivt heltal f�r starttiden f�r en lektion
	 * Klockan 00.00 => 0 och 23.59 =>  1440
	 * 
	 */
	public int getStartDayTime()
	{
		return startHour * 60 + startMin;
	}

	/**
	 * Returnerar ett positivt heltal f�r sluttiden f�r en lektion
	 * Klockan 00.00 => 0 och 23.59 =>  1440
	 * 
	 */
	public int getEndDayTime()
	{
		return endHour * 60 + endMin;
	}
	
	/**
	 * L�gger till en vy.
	 */
	@Override
	public void add(View view)
	{
		
		
	}

	/**
	 * Tar bort en vy.
	 */
	@Override
	public void remove(View view)
	{
		
		
	}

	/**
	 * Meddelar en specifik vy.
	 */
	@Override
	public void notify(View view)
	{
		
		
	}

	/**
	 * Meddelar alla vyer.
	 */
	@Override
	public void notifyAllObservers()
	{
		
		
	}

	

		
	
	

	
	
}
