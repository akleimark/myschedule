package model;

/**
 * MySchedule is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MySchedule is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MySchedule. If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import view.View;

@SuppressWarnings("serial")
@Entity
@Table(name = "SUBJECT")
public class SchoolSubject extends Model implements Serializable
{
	@Id 
    @Column(name = "SUBJECT_ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long subjectID;	
	
	
	@Column(name = "NAME", length = 32, nullable = false)
	private String name;

	
	/**
	 * F�rvald parameterl�s konstruktor
	 */
	public SchoolSubject()
	{
		
	}
	
	public SchoolSubject(String p_name)
	{
		
		name = p_name;
	}
	
	public String getName()
	{
		return name;
	}
	

	public void print()
	{
		System.out.println("�mne: " + name);
	}

	@Override
	public void add(View view)
	{
		
		
	}

	@Override
	public void remove(View view)
	{
		
		
	}

	@Override
	public void notify(View view)
	{
		
		
	}

	@Override
	public void notifyAllObservers()
	{
		
		
	}

}
	
	

