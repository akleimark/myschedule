package model;

/**
 * MySchedule is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MySchedule is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MySchedule. If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.Iterator;
import java.util.Vector;

import view.View;

public class TextModel extends Model
{
	private Vector<View> viewList;
	private String text;
	
	public TextModel()
	{
		viewList = new Vector<View>();
		
	}
	
	public void setText(String p_text)
	{
		text = p_text;
		notifyAllObservers();
	}
	
	
	@Override
	public void add(View view)
	{
		viewList.add(view);
		
	}

	@Override
	public void remove(View view)
	{
		viewList.remove(view);
		
	}

	@Override
	public void notify(View view)
	{
		view.update(text);
		
	}

	@Override
	public void notifyAllObservers()
	{
		Iterator<View> it = viewList.iterator();
		
		while(it.hasNext())
		{
			it.next().update(text);
		}
		
	}
	
}
