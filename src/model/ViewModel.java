package model;

/**
 * MySchedule is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MySchedule is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MySchedule. If not, see <http://www.gnu.org/licenses/>.
 */

import javax.swing.JPanel;

import view.View;

/**
 * Den h�r modellen v�ljer vilken vy som skall visas i applikationen.
 * @author anders
 *
 */

public class ViewModel extends Model
{
	private View view;
	private JPanel panel;
	
	 
	public ViewModel()
	{
		view = null;
		panel = new JPanel();
		
	}

	@Override
	public void add(View p_view)
	{
		view =  p_view;
		
		
	}

	@Override
	public void remove(View view)
	{
		view = null;
		
	}

	@Override
	public void notify(View p_view)
	{
		view.update(panel);
		
	}

	@Override
	public void notifyAllObservers()
	{
		
		view.update(panel);
		
		
		
	}
	
	public void setPanel(JPanel p_panel)
	{
		panel = p_panel;
		notifyAllObservers();
	}
	
}
