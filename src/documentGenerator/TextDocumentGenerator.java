package documentGenerator;

/**
 * MySchedule is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MySchedule is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MySchedule. If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.swing.JFileChooser;

import exception.ExitException;

import model.Cource;
import model.Model;

public class TextDocumentGenerator implements DocumentGenerator
{
	private List<Model> modelData;
	private BufferedWriter bufferedWriter;
	private FileWriter fileWriter;
	private File file;
	private static final JFileChooser fileChooser = new JFileChooser(); // En filv�ljare.
	
	/**
	 * Konstruktor f�r att skapa ett textdokument motsvarande schemats grafiska representation.
	 * Konstruktorn k�r metoden "generate". 
	 */
	public TextDocumentGenerator()
	{
		try
		{
			if (TextDocumentGenerator.fileChooser.showOpenDialog(null) ==  JFileChooser.APPROVE_OPTION)
			{
				file = fileChooser.getSelectedFile();			
				fileWriter = new FileWriter(file.getAbsolutePath());
				bufferedWriter = new BufferedWriter(fileWriter,1024);
			}
		}
		catch(IOException ioe)
		{
			new ExitException();
		}
			
		
	}
	
	
	/**
	 * Vi genererar med den h�r metoden ett textdokument, motsvarande den grafiska representationen 
	 * av schemat.
	 */
	@Override
	public void generate()
	{
		Iterator<Model> it = modelData.iterator();
		
		
		while(it.hasNext())
		{
			Cource cource = (Cource) it.next();
			
			StringBuffer stringBuffer = new StringBuffer(1024);
			
			stringBuffer.append("Dag: ");
			stringBuffer.append(cource.getDay().toLowerCase());
			stringBuffer.append("\nStarttid: ");
			stringBuffer.append(cource.getStartHour());
			stringBuffer.append(".");						
			stringBuffer.append(cource.getStartMin());
			if(cource.getStartMin() == 0)
			{
				stringBuffer.append("0");
			}
			stringBuffer.append("\nSluttid: ");
			stringBuffer.append(cource.getEndHour());
			stringBuffer.append(".");
			stringBuffer.append(cource.getEndMin());
			if(cource.getEndMin() == 0)
			{
				stringBuffer.append("0");
			}
			stringBuffer.append("\nSkol�mne: ");
			stringBuffer.append(cource.getSubject().getName().toLowerCase());
			stringBuffer.append("\n\n");
			
			try
			{
				bufferedWriter.write(stringBuffer.toString());				
			}
			catch (IOException e)
			{
				new ExitException();
			}
		}
		
		try
		{
			bufferedWriter.close();
		}
		catch (IOException e)
		{
			new ExitException();
		}
		
	}
	/**
	 * S�tter de modeller som anv�nds.
	 * @param data
	 */
	public void setModelData(List<Model> data)
	{
		modelData = new Vector<Model>();
		
		Iterator<Model> it = data.iterator();
		
		while(it.hasNext())
		{
			modelData.add(it.next());
		}
	}
}
