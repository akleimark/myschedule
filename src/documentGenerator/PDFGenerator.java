package documentGenerator;

/**
 * MySchedule is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MySchedule is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MySchedule. If not, see <http://www.gnu.org/licenses/>.
 */

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import view.CreateScheduleView;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;
import exception.ImageException;

public class PDFGenerator implements DocumentGenerator
{
	private static final JFileChooser fileChooser = new JFileChooser(); // En filv�ljare.
	private File file; // Ett filobjekt.
	private CreateScheduleView panel;
	private BufferedImage image;
	
	public PDFGenerator(JPanel p_panel)throws FileNotFoundException
	{		
		
		panel = (CreateScheduleView) p_panel;
		
		
		
		if (PDFGenerator.fileChooser.showOpenDialog(null) ==  JFileChooser.APPROVE_OPTION)
		{
			file = fileChooser.getSelectedFile();			
			
		}		
		
	}
	
	@Override
	public void generate()
	{
		Document document = new Document();
		try
		{
			PdfWriter.getInstance(document, new FileOutputStream(file.getAbsolutePath()));
		}
		catch (FileNotFoundException e1)
		{
			
			e1.printStackTrace();
		}
		catch (DocumentException e1)
		{
			
			e1.printStackTrace();
		}
		document.open();
		
		try
		{
			createImage();
			Image realSizedImage = Image.getInstance(image, null);
			
			realSizedImage.scaleAbsolute(580f, 600f); // Vi vill �stadkomma att schemat rymms p� en A4.
			
			float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
		               - document.rightMargin() - 20) / realSizedImage.getWidth()) * 100;
			
			realSizedImage.scalePercent(scaler);
			
			document.add(realSizedImage);	
		}
		catch (IOException | ImageException | DocumentException e)
		{
			new ImageException();			
		}
		
		document.close();
		
	}
	
	private void createImage() throws ImageException, IOException
	{
		image = new BufferedImage(panel.getWidth(), panel.getHeight(), 
				BufferedImage.TYPE_INT_ARGB);
		
		panel.paint(image.getGraphics());
				
		
	}
	
	
}
