package documentGenerator;

/**
 * MySchedule is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MySchedule is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MySchedule. If not, see <http://www.gnu.org/licenses/>.
 */
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import view.CreateScheduleView;
import exception.ImageException;

/**
 * Den h�r klassen anv�nds f�r att skapa en bild av schemat. 
 * Bilden sparas i png-formatet.
 * @Utvecklare: Anders Kleimark
 *
 */

public class ImageGenerator implements DocumentGenerator
{	
	private static final JFileChooser fileChooser = new JFileChooser(); // En filv�ljare.
	private File file; // Ett filobjekt.
	private CreateScheduleView panel;
	private BufferedImage image;

	/**
	 * Konstruktor som anv�nds f�r att skapa en ny bildfil av schemat.
	 * @param p_panel
	 * @throws FileNotFoundException
	 */
	
	public ImageGenerator(JPanel p_panel) throws FileNotFoundException
	{
		panel = (CreateScheduleView) p_panel;
		
		
		if (ImageGenerator.fileChooser.showOpenDialog(null) ==  JFileChooser.APPROVE_OPTION)
		{
			file = fileChooser.getSelectedFile();			
			
		}		
		
	}
	
	/**
	 * Skapar en ny bild.
	 */
	@Override
	public void generate()
	{
		try
		{
			createImage();
		}
		catch (IOException | ImageException e)
		{
			new ImageException();			
		}
		
		
	}
	
	/**
	 * Anv�nds av konstruktorn f�r att skapa bilden.
	 * @throws ImageException
	 * @throws IOException
	 */
	private void createImage() throws ImageException, IOException
	{
		image = new BufferedImage(panel.getWidth(), panel.getHeight(), 
				BufferedImage.TYPE_INT_ARGB);
		
		panel.paint(image.getGraphics());
		
		
		ImageIO.write(image, "PNG", file);
		
		
		JOptionPane.showMessageDialog(null, "Bilden genererades helt problemfritt.");
		
	}
	
	
	
	
}
