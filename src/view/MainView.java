package view;

/**
 * MySchedule is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MySchedule is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MySchedule. If not, see <http://www.gnu.org/licenses/>.
 */

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import client.GUI;
import controller.Controller;

@SuppressWarnings("serial")
public class MainView extends JPanel implements View
{
	private JFrame frame;
	private JPanel panel;
	
	public MainView(JFrame p_frame)
	{
		frame = p_frame;
		panel = new JPanel();
		panel.setVisible(true);
		frame.add(panel);
		frame.pack();
		frame.setVisible(true);
	}
	

	@Override
	public void update(Object object)
	{
		
		frame.remove(panel);		
		panel = (JPanel) object;
		frame.add(panel);
		frame.repaint();
		frame.setSize(GUI.getFrameWidth(), GUI.getFrameHeight());
		frame.setVisible(true);				
		
	}

	@Override
	public void render()
	{
		
		
		
	}

	@Override
	public void addController(Controller controller)
	{
		
		
	}


	@Override
	public String getViewType()
	{
		return "mainView";
	}
	
}
