package view;

/**
 * MySchedule is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MySchedule is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MySchedule. If not, see <http://www.gnu.org/licenses/>.
 */

import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import controller.Controller;

/**
 * Den h�r vyn anv�nds f�r att visa alla de f�r n�rvarande tillagda lektionerna/rasterna/matrasterna.
 * @Utvecklare: Anders Kleimark
 *
 */
@SuppressWarnings("serial")
public class ShowAllCourcesView extends JPanel implements View
{
	private JLabel header;
	private JTable table;
	
	public ShowAllCourcesView()
	{
		GridLayout layout = new GridLayout(4, 1);
		
		layout.setVgap(30);
		
		setLayout(layout);
		
	}

	/**
	 * Den h�r metoden anv�nds av kontrolleraren f�r att fylla tabellen som har data med de 
	 * tillagda lektionerna/rasterna/matrasterna.
	 * @param p_table : tabellen
	 */
	public void setTable(JTable p_table)
	{
		table = p_table;
		
	}
	
	/**
	 * Den h�r metoden skapar tabellens rubrikrad.
	 */
	private void createHeader()
	{
		header = new JLabel("Visa alla tillagda lektioner");
		
		header.setFont(new Font("Serif", Font.BOLD, 18));
		
	}

	/**
	 * Uppdaterar vyn.
	 */
	@Override
	public void update(Object object)
	{
		render();
		
	}

	/**
	 * Visar vyn.
	 */
	@Override
	public void render()
	{
		createHeader();		
		
		add(header);
		
		if(table.getRowCount() > 0)
		{
			add(table);
		}		
		
	}

	
	/**
	 * L�gger till en kontrollerare.
	 */
	@Override
	public void addController(Controller controller)
	{
		
		
	}

	/**
	 * Ger aktuell vytyp.
	 */
	@Override
	public String getViewType()
	{
		return "showAllCourcesView";
	}
	
}





