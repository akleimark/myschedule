package view;

/**
 * MySchedule is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MySchedule is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MySchedule. If not, see <http://www.gnu.org/licenses/>.
 */

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
import client.GUI;
import controller.Controller;

@SuppressWarnings("serial")
public class CreateScheduleView extends JPanel implements View
{
	private int canvasWidth;
	private int canvasHeight;
	private final int MARGINAL = 20;
	private final int HOURS = 9; // Antal visade timmar i schemat.
	private final int X_PIXELS_FOR_TIMELINE = 0;
	private final int Y_PIXELS_FOR_TIMELINE = 100;
	final int COURCE_MARGINAL = 10;
	private int xStep; // Varje dag utg�r en femtedel av bredden.
	private int yStep; // Stegl�ngden i y-led.
	private String [][] courceData;
	private final int TIME_STEP = 10;
	
	
	public CreateScheduleView()
	{		
		
		canvasWidth = (int)(GUI.getFrameWidth()*0.8 - MARGINAL); // En viss marginal kr�vs f�r att gr�nsen skall visas.
		canvasHeight = (int)(GUI.getFrameHeight()*0.85 - MARGINAL);	// En viss marginal kr�vs f�r att gr�nsen skall visas.	
		setPreferredSize(new Dimension(canvasWidth, canvasHeight));
		courceData = new String[5][100];
		xStep = (canvasWidth - X_PIXELS_FOR_TIMELINE - 2 * MARGINAL) / 6;
		yStep = (canvasHeight - Y_PIXELS_FOR_TIMELINE)/(HOURS * (60/TIME_STEP)); // 54 = HOURS * (60/TIME_STEP)
	}
	
	
	@Override
	public void update(Object object)
	{			
		
		
		canvasWidth = (int)(GUI.getFrameWidth()*0.8 - MARGINAL); // En viss marginal kr�vs f�r att gr�nsen skall visas.
		canvasHeight = (int)(GUI.getFrameHeight()*0.85 - MARGINAL);	// En viss marginal kr�vs f�r att gr�nsen skall visas.	
		setPreferredSize(new Dimension(canvasWidth, canvasHeight));
		xStep = (canvasWidth - X_PIXELS_FOR_TIMELINE - 2 * MARGINAL) / 6;
		yStep = (canvasHeight - Y_PIXELS_FOR_TIMELINE)/(HOURS * (60/TIME_STEP));
		
	}

	@Override
	public void render()
	{		
		
		
	}

	@Override
	public void addController(Controller controller)
	{
		
		
	}
	
	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponents(g);
		
		Graphics2D g2 = (Graphics2D) g;
		
		g2.setColor(Color.BLACK);
		
		createBorder(g2);
		
		createTimeLine(g2);
		
		createDays(g2);
		
		createOriginalGrid(g2);
		
		addCourcesToSchedule(g2);
				
		
		
	}
	/**
	 * En grafisk funktion som skapar den omkringliggande "border" f�r schemat.
	 * @param g2
	 */
	private void createBorder(Graphics2D g)
	{
		g.setStroke(new BasicStroke(3));
		g.setColor(Color.BLACK);
		int xStart = 0;
		int xEnd = canvasWidth; 
		int yStart = 0;
		int yEnd = canvasHeight;
		
		g.drawRect(xStart, yStart, xEnd, yEnd); 
		
	}

	/**
	 * FUnktionen skapar linjer vid lektionerna, s� att det blir enklare att f�lja schemat.
	 * @param g
	 */
	
	private void createOriginalGrid(Graphics2D g)
	{
		int x = X_PIXELS_FOR_TIMELINE;
		int y = Y_PIXELS_FOR_TIMELINE;
		
		final int STEP = (canvasHeight - Y_PIXELS_FOR_TIMELINE)/54;
		
		while(y < canvasHeight)
		{
			g.drawLine(x, y, canvasWidth, y);
			
			y += STEP;
		}
	}

	/**
	 * Skapar tidsaxeln p� y-axeln.
	 * @param g
	 */
	private void createTimeLine(Graphics2D g)
	{
		final int x = MARGINAL;
				
		int y = Y_PIXELS_FOR_TIMELINE;
		
		int startHour = 7;
		
		
		
		while(startHour++ < 16)		
		{
			g.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 9));
			g.drawString(Integer.toString(startHour) + "." + "00", x, y + 10);
			g.drawString(Integer.toString(startHour) + "." + "00", x + 10 * xStep + 5 * COURCE_MARGINAL, y + 10);
			y+= (60/TIME_STEP) * yStep;																				
			
		}
				
		
	}
	
	/**
	 * Skapar etiketterna f�r veckodagarna. Dessa placeras ovanf�r schemat.
	 * @param g
	 */
	private void createDays(Graphics2D g)
	{	
		g.setFont(new Font(Font.MONOSPACED, Font.BOLD, 12));
		final int y = Y_PIXELS_FOR_TIMELINE/2;
		
		int x = X_PIXELS_FOR_TIMELINE + 2 * xStep/3;
		
		
		String[] days = {"M�ndag", "Tisdag", "Onsdag", "Torsdag", "Fredag"};
		
		for(int i = 0; i < 5; i++)
		{
			g.drawString(days[i], x, y);
			
			x += xStep;
			
		}
	}
	
	public void setCourceData(String day, String courceName, int startHour, int startMin, int endHour, int endMin)
	{		
		
		int dayIndex = 0;
		int startIndex = 0;
		int endIndex = 0;		
		
		switch(day)
		{
			case "M�ndag" :
				dayIndex = 0;				
				break;
				
			case "Tisdag" :
				dayIndex = 1;
				break;	
			
			case "Onsdag" :
				dayIndex = 2;
				break;	
			
			case "Torsdag" :
				dayIndex = 3;
				break;
			
			case "Fredag" :
				dayIndex = 4;
				break;								
			
		}
				
		
		int startH = 8;
		int startM = 0;
		
		while(!(startH == startHour && startM == startMin))
				
		{
			startIndex++;
			startM += TIME_STEP;
					
			if(startM == 60)
			{
				startH++;
				startM = 0;
			}
			
			
		}
		
		
		
		int endH = 8;
		int endM = 0;
		
		while(!(endH == endHour && endM == endMin))
		{
			endIndex++;
			endM += TIME_STEP;
			
			if(endM == 60)
			{
				endH++;
				endM = 0;
			}
						
		}
		
		while(startIndex++ < endIndex - 1)
		{
			courceData[dayIndex][startIndex] = courceName;
			
		}
						
	}
	
	
	public void addCourcesToSchedule(Graphics2D g)
	{
		int x0 = 0; // startv�rde f�r den rektangel som skall omge lektionen (x - koordinaten)
		int x1 = 0; // slutv�rde f�r den rektangel som skall omge lektionen (x - koordinaten)
		int y0 = 0; // startv�rde f�r den rektangel som skall omge lektionen (y - koordinaten)
		int y1 = 0; // slutv�rde f�r den rektangel som skall omge lektionen (y - koordinaten)
		boolean newCource = true;
		
		for(int i = 0; i < 5; i++) // Alla veckodagarna g�s igenom (index 0 => m�ndag, 1 => tisdag o.s.v).
		{			
			
			for(int j = 0; j < 100; j++)
			{
				Point point = getCoordinatesFromCourceData(i, j);
				if(newCource)
				{
					x0 = point.x;
					y0 = point.y;
					x1 = point.x + xStep;
					y1 = point.y;
				}
				
				
				if(courceData[i][j] == null)
				{
					continue;
				}
				
				if(courceData[i][j].equals(courceData[i][j + 1]))
				{
					newCource = false;					
					y1 += yStep;
					continue;
				}
				
				if(!courceData[i][j].equals("Rast") && !courceData[i][j].equals("Lunch"))
				{
					g.setColor(Color.WHITE);
					g.fillRect(x0 + COURCE_MARGINAL, y0 - yStep, x1 - x0, y1 - y0 + 2* yStep);
					g.setColor(Color.black);
					g.drawString(courceData[i][j], x0 +  2 * COURCE_MARGINAL, y0);
					g.drawRect(x0 + COURCE_MARGINAL, y0 - yStep, x1 - x0, y1 - y0 + 2* yStep);												
					
					File file = new File("images/" + courceData[i][j] + ".jpg");
					
					BufferedImage bufferImage = new BufferedImage(100, 100, BufferedImage.TYPE_INT_ARGB);
					try
					{
						bufferImage = ImageIO.read(file);
						g.drawImage(bufferImage, null, x1 -bufferImage.getWidth() , y0);						
						newCource = true;
					}
					catch (IOException e)
					{
						newCource = true;
						continue;
					}

					
				}
				else
				{
					g.setColor(Color.BLACK);
					g.fillRect(x0 + COURCE_MARGINAL, y0 - yStep, x1 - x0, y1 - y0 + 2* yStep);
					g.drawRect(x0 + COURCE_MARGINAL, y0 - yStep, x1 - x0, y1 - y0 + 2* yStep);
					newCource = true;
				}
				
				
			}
			
			
		}
		
		
	}
	
	private Point getCoordinatesFromCourceData(int dayIndex, int timeIndex)
	{
		
		int xCoordinate = X_PIXELS_FOR_TIMELINE + xStep/3 + xStep * dayIndex; // x-koordinaten.
		int yCoordinate = Y_PIXELS_FOR_TIMELINE + (timeIndex) * ((canvasHeight - Y_PIXELS_FOR_TIMELINE)/54); // y-koordinaten.
		
		return new Point(xCoordinate, yCoordinate);
	}
	
	public int getWidth()
	{
		return canvasWidth;
	}
	
	public int getHeight()
	{
		return canvasHeight;
	}


	@Override
	public String getViewType()
	{
		return "createScheduleView";
	}
	
	
}
