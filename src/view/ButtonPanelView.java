package view;

/**
 * MySchedule is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MySchedule is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MySchedule. If not, see <http://www.gnu.org/licenses/>.
 */

import java.awt.GridLayout;
import java.util.Iterator;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JPanel;
import controller.Controller;

@SuppressWarnings("serial")
public class ButtonPanelView extends JPanel implements View
{
	private Vector<JButton> buttons;
	 
	
	public ButtonPanelView()
	{
		this.setLayout(new GridLayout(0,4));
		buttons = new Vector<JButton>();
	
		buttons.add(new JButton("Om applikationen"));
		buttons.add(new JButton("L�gg till en lektion/rast/lunch"));
		buttons.add(new JButton("Visa alla data"));
		buttons.add(new JButton("Visa schemat"));
		buttons.add(new JButton("Generera schemabild"));
		buttons.add(new JButton("Generera textdokument"));
		buttons.add(new JButton("Generera pdf-dokument"));
		
	}
	
	

	@Override
	public void update(Object object)
	{
		
		
	}

	@Override
	public void render()
	{
		Iterator<JButton> it = buttons.iterator();
		
		while(it.hasNext())
		{
			JButton button = it.next();
			add(button);
			button.setVisible(true);
		}
		
		setVisible(true);
		
	}



	@Override
	public void addController(Controller controller)
	{
		Iterator<JButton> it = buttons.iterator();
		
		String[] itemNames = {"aboutApplication", "newCource", "showAllCources", "showSchedule", "image", 
				"text", "pdf"};
		int index = 0;
		
		while(it.hasNext())
		{
			JButton button = it.next();
			button.addActionListener(controller);
			button.setName(itemNames[index++]);
		}
		
		
		
	}



	@Override
	public String getViewType()
	{
		return "buttonPanel";
	}
	
}
