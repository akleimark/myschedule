package view;

/**
 * MySchedule is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MySchedule is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MySchedule. If not, see <http://www.gnu.org/licenses/>.
 */

import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import client.GUI;

import controller.Controller;

@SuppressWarnings("serial")
public class AboutApplicationView extends JPanel implements View
{
	private JTextArea textArea;

	public AboutApplicationView()
	{				
		
		textArea = new JTextArea("MySchedule �r ett schemaprogram, med vilket det g�r att \nskapa egna veckoscheman f�r en " +
		 		"skolklass.\n\nProgrammering och design: @Anders Kleimark");
         
		 textArea.setEditable(false);
		 textArea.setFont(new Font("Serif", Font.ITALIC, 18));
		 
		 setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
		
	}
	
	@Override
	public void update(Object object)
	{
		
		
	}

	@Override
	public void render()
	{
		add(textArea);
		
	}

	@Override
	public void addController(Controller controller)
	{
		
		
	}

	@Override
	public String getViewType()
	{
		return "aboutApplicationView";
	}
	
}
