package view;

/**
 * MySchedule is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MySchedule is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MySchedule. If not, see <http://www.gnu.org/licenses/>.
 */

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import model.Model;
import model.SchoolSubject;
import controller.Controller;

@SuppressWarnings("serial")
public class NewCourceView extends JPanel implements View
{
	private JLabel header; //Rubrik
	private List<Model> databaseResults;
	private GridBagLayout layout; // Vi anv�nder en "GridBagLayout".
	private GridBagConstraints constraints; // V�ra "GridBagConstraints".
	private JComboBox<Integer> startHourList;
	private JComboBox<Integer> startMinList;
	private JComboBox<Integer> endHourList;
	private JComboBox<Integer> endMinList;
	private JComboBox<String> schoolSubjectList;
	private JComboBox<String> dayList;
	private JButton saveButton;
	
	
	public NewCourceView()
	{
		layout = new GridBagLayout();
		constraints = new GridBagConstraints();
		
		constraints.insets = new Insets(20,20,20,20);
						
		databaseResults = new ArrayList<Model>();
	}
	
	
	public void setDatabaseResults(List<Model> results)
	{
		databaseResults = results;
	}
	

	@Override
	public void update(Object object)
	{
		
		
	}

	@Override
	public void render()
	{
		setLayout(layout);
		
		createHeader();
		
		createSubjectLabel();
		
		createSubjectComboList();
		
		createStartHour();
		
		createStartMin();
		
		createEndHour();
		
		createEndMin();
		
		createDayList();
		
		createButtons();
		
	}
	
	/**
	 * F�r att skapa sidans rubrik.
	 */
	private void createHeader()
	{
		header = new JLabel("L�gg till en lektion");
		
		header.setFont(new Font("Serif", Font.BOLD, 18));
		
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.fill = GridBagConstraints.CENTER;
		constraints.weightx = 0.0;
		constraints.gridwidth = 4;
		
		layout.setConstraints(header, constraints);
		
		constraints = new GridBagConstraints(); // De �vriga komponenterna �r inte centrerade.
		constraints.anchor = GridBagConstraints.FIRST_LINE_START;
		constraints.insets = new Insets(20,20,20,20);
		
		add(header);
		
	}
	
	/**
	 * F�r att skapa etiketten med val av skol�mne.
	 */
	private void createSubjectLabel()
	{
				
		JLabel label = new JLabel("Ange skol�mnet: ");
		
		label.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 16));
		
		constraints.gridx = 0;
		
		constraints.gridy = 1;
		
		layout.setConstraints(label, constraints);
		
		
		
		add(label);
		
		
	}
	
	/**
	 * Den h�r funktionen skapar den "JComboBox", som anv�nds f�r att visa de tillg�ngliga skol�mnena.
	 */
	private void createSubjectComboList()
	{
		Vector<String> vectorList = new Vector<String>();
		
		Iterator<Model> it = databaseResults.iterator();
		
		while(it.hasNext())
		{
			SchoolSubject subject = (SchoolSubject) it.next();
			
			String subjectName = subject.getName();
			
			vectorList.add(subjectName);
			
		}
		
		java.util.Collections.sort(vectorList);
				
		schoolSubjectList = new JComboBox<String>(vectorList);
		
		constraints.gridx = 1;
		
		constraints.gridy = 1;
		
		layout.setConstraints(schoolSubjectList, constraints);
		
		
		
		add(schoolSubjectList);
		
	}
	
	
	private void createStartHour()
	{
		
		
		JLabel label = new JLabel("Ange starttimme: ");
		
		label.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 16));
		
		constraints.gridx = 0;
		
		constraints.gridy = 2;
		
		layout.setConstraints(label, constraints);
		
		add(label);
		
		Vector<Integer> hours = new Vector<Integer>();
		
		for(int i = 8; i < 16; i++)
		{
			hours.add(i);
		}
		
		startHourList = new JComboBox<Integer>(hours);
		
		constraints.gridx = 1;
		
		constraints.gridy = 2;
		
		layout.setConstraints(startHourList, constraints);
		
		add(startHourList);
		
	}
	
	private void createEndHour()
	{
		
		
		JLabel label = new JLabel("Ange sluttimme: ");
		
		label.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 16));
		
		constraints.gridx = 0;
		
		constraints.gridy = 4;
		
		layout.setConstraints(label, constraints);
		
		add(label);
		
		Vector<Integer> hours = new Vector<Integer>();
		
		for(int i = 8; i < 16; i++)
		{
			hours.add(i);
		}
		
		endHourList = new JComboBox<Integer>(hours);
		
		constraints.gridx = 1;
		
		constraints.gridy = 4;
		
		layout.setConstraints(endHourList, constraints);
		
		add(endHourList);
		
	}

	private void createStartMin()
	{
		
		
		JLabel label = new JLabel("Ange startminut: ");
		
		label.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 16));
		
		constraints.gridx = 0;
		
		constraints.gridy = 3;
		
		layout.setConstraints(label, constraints);
		
		add(label);
		
		Vector<Integer> min = new Vector<Integer>();
		
		for(int i = 0; i < 60; i+=10)
		{
			min.add(i);
		}
		
		startMinList = new JComboBox<Integer>(min);
		
		constraints.gridx = 1;
		
		constraints.gridy = 3;
		
		layout.setConstraints(startMinList, constraints);
		
		add(startMinList);
		
	}
	
	private void createEndMin()
	{
		
		
		JLabel label = new JLabel("Ange slutminut: ");
		
		label.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 16));
		
		constraints.gridx = 0;
		
		constraints.gridy = 5;
		
		layout.setConstraints(label, constraints);
		
		add(label);
		
		Vector<Integer> min = new Vector<Integer>();
		
		for(int i = 0; i < 60; i+=10)
		{
			min.add(i);
		}
		
		endMinList = new JComboBox<Integer>(min);
		
		constraints.gridx = 1;
		
		constraints.gridy = 5;
		
		layout.setConstraints(endMinList, constraints);
		
		add(endMinList);
		
	}
	
	private void createDayList()
	{
		JLabel label = new JLabel("Ange dag f�r lektionen: ");
		
		label.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 16));
		
		constraints.gridx = 0;
		
		constraints.gridy = 6;
		
		layout.setConstraints(label, constraints);
		
		add(label);
		
		Vector<String> dayVector = new Vector<String>();
		
		dayVector.add("M�ndag");
		dayVector.add("Tisdag");
		dayVector.add("Onsdag");
		dayVector.add("Torsdag");
		dayVector.add("Fredag");
		
		
		
		dayList = new JComboBox<String>(dayVector);
		
		constraints.gridx = 1;
		
		constraints.gridy = 6;
		
		layout.setConstraints(dayList, constraints);
		
		add(dayList);
				
	}
	
	/**
	 * F�r att skapa "Avbryt-knappen" och "Spara".
	 */
	private void createButtons()
	{
		saveButton = new JButton("Spara");
		
		constraints.gridx = 1;
		
		constraints.gridy = 7;
		
		layout.setConstraints(saveButton, constraints);
		
		add(saveButton);
		
	}
	
	public int getStartHour()
	{
		return (int)startHourList.getSelectedItem();
	}
	
	public int getEndHour()
	{
		return (int)endHourList.getSelectedItem();
	}
	
	public int getStartMin()
	{
		return (int)startMinList.getSelectedItem();
	}
	
	public int getEndMin()
	{
		return (int)endMinList.getSelectedItem();
	}
	
	public String getSubjectString()
	{
		return (String)schoolSubjectList.getSelectedItem();
	}
	
	public String getDay()
	{
		return (String)dayList.getSelectedItem();
	}
	
	
	@Override
	public void addController(Controller controller)
	{
		schoolSubjectList.addItemListener(controller);
		startHourList.addItemListener(controller);
		startMinList.addItemListener(controller);
		endHourList.addItemListener(controller);
		endMinList.addItemListener(controller);
		dayList.addItemListener(controller);
		saveButton.addActionListener(controller);		
		
		
		schoolSubjectList.setName("schoolSubjectList");
		startHourList.setName("startHourList");
		startMinList.setName("startMinList");
		endHourList.setName("endHourList");
		endMinList.setName("endMinList");
		dayList.setName("dayList");
		saveButton.setName("saveButton");		
		
	}


	@Override
	public String getViewType()
	{
		return "newCourceView";
	}
	
}
