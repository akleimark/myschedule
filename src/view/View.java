package view;

/**
 * MySchedule is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MySchedule is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MySchedule. If not, see <http://www.gnu.org/licenses/>.
 */


import controller.Controller;

/**
 * Ett gr�nssnitt som alla vyer implementerar. Gr�nssnittet �rver i sin tur gr�nssnittet Observer.
 * @utvecklare: Anders Kleimark
 *
 */

public interface View extends Observer
{
	public abstract void render(); /** Visar vyn. */
	public void addController(Controller controller); /** L�gger till en kontrollerare till vyn. */
	public String getViewType(); /** Den h�r metoden ger aktuell vytyp.*/
}
