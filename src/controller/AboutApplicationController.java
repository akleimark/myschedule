package controller;
/**
 * MySchedule is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MySchedule is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MySchedule. If not, see <http://www.gnu.org/licenses/>.
 */

import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;

import javax.swing.JPanel;

import view.AboutApplicationView;
import factory.AboutApplicationViewFactory;

public class AboutApplicationController extends Controller
{
	public AboutApplicationController()
	{
		Controller.viewFactory = new AboutApplicationViewFactory();
		Controller.panel = (JPanel) Controller.viewFactory.createProduct();
		((AboutApplicationView) Controller.panel).render();
	}
	

	@Override
	public void actionPerformed(ActionEvent e)
	{
		
		
	}

	@Override
	public void itemStateChanged(ItemEvent e)
	{
		
		
	}
	
}
