package controller;
/**
 * MySchedule is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MySchedule is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MySchedule. If not, see <http://www.gnu.org/licenses/>.
 */
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import model.Cource;
import model.Model;
import view.View;
import exception.DatabaseException;

public class ActionController extends Controller
{
	private JTable table;
	
	public ActionController(View p_panel, JTable p_table)
	{
		table = p_table;
	}
	
	@Override
	public void actionPerformed(ActionEvent e)
	{
		Controller.database.createQuery("select x from Cource x order by x.day, x.startHour, x.startMin");
		
		List<Model> result;
		try
		{
			result = Controller.database.getResult();
			
			int option = JOptionPane.showConfirmDialog(null,"�r du s�ker?");
			
			if(option == JOptionPane.YES_OPTION)
	    	{
	    		Cource cource = ((Cource) result.get(table.getSelectedRow()));
	        		           
	    		Controller.database.checkActivity();	            		
	    		Controller.database.getEntityManager().remove(cource);	            		
	    		Controller.database.getEntityManager().getTransaction().commit();
	    		Controller.database.checkActivity();	 
	    		Controller.database.getEntityManager().flush();
	    		
	    		JOptionPane.showMessageDialog(null, "Lektionen raderades.");
	    		
	    		// Vi uppdaterar.
	    		
	    		((DefaultTableModel)table.getModel()).removeRow(table.getSelectedRow());
	    		
	    	}
		}
		catch (DatabaseException e1)
		{
			new DatabaseException();
		}
		
			
		
	}

	@Override
	public void itemStateChanged(ItemEvent e)
	{
		
		
	}
}
