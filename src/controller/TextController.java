package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.util.List;

import javax.swing.JOptionPane;

import documentGenerator.TextDocumentGenerator;

import exception.DatabaseException;

import model.Model;

/**
 * MySchedule is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MySchedule is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MySchedule. If not, see <http://www.gnu.org/licenses/>.
 */

public class TextController extends Controller
{
	public TextController()
	{
		List<Model> result = null;
		Controller.database.createQuery("select x from Cource x order by x.day, x.startHour, x.startMin");
		
		try
		{
			result = Controller.database.getResult();
		}
		catch (DatabaseException e1)
		{
			new DatabaseException();
		}
		
		
		final TextDocumentGenerator generator = new TextDocumentGenerator();
		generator.setModelData(result);
		
		Runnable runnable = new Runnable()
		{

			@Override
			public void run()
			{
				generator.generate();				
			}
			
		};
		
		Thread t = new Thread(runnable);
		t.start();  
		
		JOptionPane.showMessageDialog(panel, "Textdokumentet genereras.");
		
		
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e)
	{
		
		
	}
	
	@Override
	public void itemStateChanged(ItemEvent e)
	{
		
		
	}
	
}
