package controller;
/**
 * MySchedule is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MySchedule is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MySchedule. If not, see <http://www.gnu.org/licenses/>.
 */
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import javax.swing.JButton;
import model.Model;
import model.ViewModel;
import view.CreateScheduleView;
import view.NewCourceView;
import view.ShowAllCourcesView;
import exception.DatabaseException;
import factory.GraphicControllerFactory;
import factory.ImageControllerFactory;
import factory.NewCourceControllerFactory;
import factory.PDFControllerFactory;
import factory.SaveCourceControllerFactory;
import factory.ShowAllCourcesControllerFactory;
import factory.TextControllerFactory;


/**
 * Den i s�rklass st�rsta kontrolleraren. 
 * @Utvecklare:  Anders Kleimark
 *
 */

public class HomeController extends Controller
{		
	private Model model;
	private String viewType;

	public HomeController()
	{
		
	}
	
	@Override
	public synchronized void actionPerformed(ActionEvent e)
	{
		JButton item = (JButton) e.getSource();
		String itemName = item.getName();
		
		switch(itemName)
		{
			// Visar information om applikationen.
			case "aboutApplication":				
				
				Controller.controllerFactory = new AboutApplicationControllerFactory();
				try
				{
					Controller.controllerFactory.createProduct();
				}
				catch (DatabaseException e3)
				{
					
					e3.printStackTrace();
				}
				viewType = "aboutApplication";
				break;	
			 
			// Visar vyn f�r att l�gga till en ny lektion. 	
			case "newCource":
							
				Controller.controllerFactory = new NewCourceControllerFactory();
				try
				{
					Controller.controllerFactory.createProduct();
				}
				catch (DatabaseException e3)
				{
					
					e3.printStackTrace();
				}
				viewType = (String) ((NewCourceView) panel).getViewType();								
				
				break;
			
			// Visar alla tillagda kurser.	
			case "showAllCources":
				
				Controller.controllerFactory = new ShowAllCourcesControllerFactory();
				try
				{
					Controller.controllerFactory.createProduct();
				}
				catch (DatabaseException e3)
				{
					
					e3.printStackTrace();
				}
				viewType = (String) ((ShowAllCourcesView) panel).getViewType();	
				
				break;
				
			
			// Visar schemat.	
			case "showSchedule":
				
				
				Controller.controllerFactory = new GraphicControllerFactory();
				try
				{
					Controller.controllerFactory.createProduct();
				}
				catch (DatabaseException e3)
				{
					
					e3.printStackTrace();
				}
				viewType = (String) ((CreateScheduleView) panel).getViewType();	
				
				((CreateScheduleView) panel).addController(this); // Vi l�gger till en "HomeController".
				
				break;
			 
			// F�r att spara en lektion	
			case "saveButton":
				
				Controller.controllerFactory = new SaveCourceControllerFactory();
				try
				{
					Controller.controllerFactory.createProduct();
				}
				catch (DatabaseException e3)
				{
					
					e3.printStackTrace();
				}
				
				
				viewType = "saveButton";
				
				break;
				
			//	F�r att generera en schemabild.
			case "image":
																															
				Controller.controllerFactory = new ImageControllerFactory();
				try
				{
					Controller.controllerFactory.createProduct();
				}
				catch (DatabaseException e3)
				{
					
					e3.printStackTrace();
				}
					
				viewType = "image";
				
				break;
				
			
			case "text":
				
				Controller.controllerFactory = new TextControllerFactory();
				try
				{
					Controller.controllerFactory.createProduct();
				}
				catch (DatabaseException e3)
				{
					
					e3.printStackTrace();
				}
				
				panel = null;	
				viewType = "text";
				
				break;
				
			case "pdf":
				
				Controller.controllerFactory = new PDFControllerFactory();
				try
				{
					Controller.controllerFactory.createProduct();
				}
				catch (DatabaseException e3)
				{
					
					e3.printStackTrace();
				}
				
					
				viewType = "pdf";
				panel = null;	
				
				break;	
								
			
		}
	
		
		if(panel != null && !viewType.equals("image") && !viewType.equals("saveButton"))
		{
			((ViewModel) model).setPanel(panel);
		}
		
		
	}
	

	@Override
	public void itemStateChanged(ItemEvent e)
	{
		
	}
	
	public void setModel(Model p_model)
	{
		model = p_model;
	}

	
}
