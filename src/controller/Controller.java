package controller;
/**
 * MySchedule is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MySchedule is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MySchedule. If not, see <http://www.gnu.org/licenses/>.
 */
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;

import javax.swing.JPanel;

import factory.ControllerFactory;
import factory.ViewFactory;
import database.Database;

/**
 * 
 * Den abstrakta klassen "Controller" utg�r basklass f�r alla andra kontrollerare. 
 * Dessa kontrollerare �rver fr�n den h�r klassen. Alla kontrollerare anv�nder ocks� samma instans av 
 * databasen. Ut�ver detta �r alla kontrollerare lyssnare. De implementerar s�ledes gr�nssnittet 
 * "ActionListener" och "ItemListener".
 * @Utvecklare: Anders Kleimark
 *
 */

public abstract class Controller implements ActionListener, ItemListener
{
	protected static JPanel panel;
	protected static Database database;
	protected static ViewFactory viewFactory;
	protected static ControllerFactory controllerFactory;
	
	protected Controller()
	{
		if(Controller.database == null)
		{
			Controller.database = new Database();
		}
		
		if(Controller.panel == null)
		{
			Controller.panel = new JPanel();
		}
	}
	
}
