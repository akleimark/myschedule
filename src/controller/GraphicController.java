package controller;
/**
 * MySchedule is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MySchedule is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MySchedule. If not, see <http://www.gnu.org/licenses/>.
 */
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.util.Iterator;
import java.util.List;
import javax.swing.JPanel;
import model.ApplicationDataModel;
import model.Cource;
import model.Model;
import view.CreateScheduleView;
import view.View;
import exception.DatabaseException;
import extra.JLabelWait;
import factory.CreateScheduleViewFactory;

public class GraphicController extends Controller
{
	private Model model;
	
	public GraphicController()
	{
		JLabelWait label = new JLabelWait(); // Visar "v�nta" om databasen m�ste initieras.
		
		
		Controller.viewFactory = new CreateScheduleViewFactory();
		Controller.panel = (JPanel) Controller.viewFactory.createProduct();
				
		
		ApplicationDataModel applicationDataModel = new ApplicationDataModel();
		applicationDataModel.add((View) panel);
		
			
		Controller.panel.add(label);
												
		
		label.setVisible(false);
				
		setModel(applicationDataModel);
		((CreateScheduleView) panel).addController(this); // Vi l�gger till en grafiklyssnare.
		
		
		try
		{
			setCources();
		}
		catch (DatabaseException e1)
		{
			new DatabaseException();
		}
		
		((CreateScheduleView) panel).render();
		
		
	}
	
	/**
	 * Skickar lektionsv�rden till vyn.
	 * @throws DatabaseException
	 */
	public void setCources() throws DatabaseException
	{
		
		Controller.database.createQuery("select x from Cource x order by x.day, x.startHour, x.startMin");
		List<Model> result = Controller.database.getResult();
		
		Iterator<Model> it = result.iterator();
		
		while(it.hasNext())
		{
			Cource model = (Cource) it.next();
			
			String day = model.getDay();
			
			int startHour = model.getStartHour();
			
			int endHour = model.getEndHour();
			
			int startMin = model.getStartMin();
			
			int endMin = model.getEndMin();
			
			String schoolSubjectName = model.getSubject().getName();
			
			((CreateScheduleView) panel).setCourceData(day, schoolSubjectName, startHour, startMin, endHour, endMin);
			
		}
				
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e)
	{
		
		
	}

	@Override
	public void itemStateChanged(ItemEvent e)
	{
		
		
	}
	
	
	public void setModel(Model p_model)
	{
		model = p_model;
	}
	
}
