package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ItemEvent;
import javax.swing.JFrame;
import model.ApplicationDataModel;
import model.Model;

public class FrameSizeController extends Controller implements ComponentListener
{
	Model model;
	
	
	@Override
	public void componentResized(ComponentEvent e)
	{						
		JFrame frame = (JFrame) e.getSource();				
				
		
		((ApplicationDataModel) model).setFrameDimension(frame.getWidth(), frame.getHeight());
				
				
	}

	@Override
	public void componentMoved(ComponentEvent e)
	{
		
		
	}

	@Override
	public void componentShown(ComponentEvent e)
	{
		
		
	}

	@Override
	public void componentHidden(ComponentEvent e)
	{
		
		
	}
	
	public void setModel(Model p_model)
	{
		model = p_model;
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		
		
	}

	@Override
	public void itemStateChanged(ItemEvent e)
	{
		
		
	}
	
	
}
