package controller;

/**
 * MySchedule is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MySchedule is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MySchedule. If not, see <http://www.gnu.org/licenses/>.
 */
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.util.List;
import java.util.Vector;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import model.Cource;
import model.Model;
import view.ShowAllCourcesView;
import view.View;
import exception.DatabaseException;
import factory.ShowAllCourcesViewFactory;

public class ShowAllCourcesController extends Controller
{
	private JTable table;
	private List<Model> result;
	
	public ShowAllCourcesController() throws DatabaseException
	{
		Controller.viewFactory = new ShowAllCourcesViewFactory();
		Controller.panel = (JPanel) Controller.viewFactory.createProduct();
		
		setTable();
		((ShowAllCourcesView) panel).setTable(table);
		
		((ShowAllCourcesView) panel).render();
		
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e)
	{
		
		
	}
	
	@Override
	public void itemStateChanged(ItemEvent e)
	{
		
		
	}
	
	/**
	 * F�r att skapa den tabell med de tidigare tillagda lektionerna, som visas i vyn "ShowAllCOurcesView". 
	 * @throws DatabaseException 
	 */
	public void setTable() throws DatabaseException
	{
		Vector<String> tHead = new Vector<String>(); 
		Vector<Vector<String>> tData = new Vector<Vector<String>>(); 
		
		tHead.add("Dag");
		tHead.add("Skol�mne");
		tHead.add("Starttid");
		tHead.add("Sluttid");
		
		Controller.database.createQuery("select x from Cource x order by x.day, x.startHour, x.startMin");
		result = Controller.database.getResult();
		
		for(int i = 0; i < result.size(); i++)
		{
			Cource cource = ((Cource) result.get(i));
			String day = cource.getDay();
			String subject = cource.getSubject().getName();
			String startTime = cource.getStartHour() + ".";
			
			if(cource.getStartMin() < 10)
			{
				startTime += "0" + cource.getStartMin();
			}
			else
			{
				startTime += cource.getStartMin();
			}
				
			String endTime = cource.getEndHour() + ".";
			
			if(cource.getEndMin() < 10)
			{
				endTime += "0" + cource.getEndMin();
			}
			
			else
			{
				endTime += cource.getEndMin();
			}
				
			Vector<String> row = new Vector<String>();
			row.add(day);
			row.add(subject);
			row.add(startTime);
			row.add(endTime);
			
			tData.add(row);
		}
			
		table = new JTable(tData, tHead);
					
		
		for(int j = 0; j < table.getColumnCount(); j++)
		{
			table.getColumnModel().getColumn(j).setPreferredWidth(100);				
		}
		
		
		final JPopupMenu popupMenu = new JPopupMenu();
		
        JMenuItem deleteItem = new JMenuItem("Radera");
        
        deleteItem.addActionListener(new ActionController((View) panel, table)); 
		
        popupMenu.add(deleteItem);
        table.setComponentPopupMenu(popupMenu);
		
	}
	
	
}
