package controller;

/**
 * MySchedule is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MySchedule is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MySchedule. If not, see <http://www.gnu.org/licenses/>.
 */

import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.io.FileNotFoundException;
import documentGenerator.PDFGenerator;
import exception.DatabaseException;
import factory.GraphicControllerFactory;

public class PDFController extends Controller
{
	public PDFController()throws FileNotFoundException
	{
		PDFGenerator pdfGenerator = null;
		
		Controller.controllerFactory = new GraphicControllerFactory();
		try
		{
			Controller gController = Controller.controllerFactory.createProduct();
			((GraphicController) gController).setCources();
			
			
			
			pdfGenerator = new PDFGenerator(panel);
			pdfGenerator.generate();
		}
		catch (DatabaseException e3)
		{
			
			e3.printStackTrace();
		}
		
		
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e)
	{
		
		
	}
	
	@Override
	public void itemStateChanged(ItemEvent e)
	{
		
		
	}
	
}



