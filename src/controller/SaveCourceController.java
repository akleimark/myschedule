package controller;

/**
 * MySchedule is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MySchedule is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MySchedule. If not, see <http://www.gnu.org/licenses/>.
 */

import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JOptionPane;

import model.Cource;
import model.Model;
import model.SchoolSubject;
import view.NewCourceView;
import exception.DatabaseException;
import exception.ExitException;

public class SaveCourceController extends Controller
{
	private final int SHORTEST_COURCE_LENGTH = 20; // Lektioner kan inte vara kortare �n 20 minuter.
	
	public SaveCourceController() throws DatabaseException, ExitException
	{
		saveAction();
	}
	
	/**
	 * F�r att f�rs�ka spara en ny lektion i databasen.
	 * @throws DatabaseException 
	 */
	private void saveAction() throws DatabaseException, ExitException
	{
		SchoolSubject subject = null;
		String subjectName = ((NewCourceView) panel).getSubjectString(); // �mnets namn
		
			
				
		Controller.database.createQuery("SELECT x FROM SchoolSubject x");
		List<Model> result = Controller.database.getResult();
				
				
		for(int i = 0; i < result.size(); i++)
		{
			// Samma namn p� lektionen => tr�ff
			if(subjectName.equals(((SchoolSubject) result.get(i)).getName()))
			{
				subject = (SchoolSubject) result.get(i);
			}
		}
		
		String day = ((NewCourceView) panel).getDay();
		int startHour = ((NewCourceView) panel).getStartHour();
		int endHour = ((NewCourceView) panel).getEndHour();
		int startMin = ((NewCourceView) panel).getStartMin();
		int endMin = ((NewCourceView) panel).getEndMin();
		
		
		Cource cource = new Cource(day, startHour, startMin, endHour, endMin);			
		
		cource.setSubject((subject));
		
		if(!checkData())
		{
			JOptionPane.showMessageDialog(null, "Du angav inkonsekventa data. Sluttiden m�ste ligga l�ngre fram i tiden �n starttiden.");
			
		}
		
		else if(courceOverlap(cource))
		{
			JOptionPane.showMessageDialog(panel, "En annan lektion finns redan p� den h�r tiden.");
		}
		
		// Alla lektioner m�ste vara minst 20 minuter l�nga.
		else if(!checkCourceLength(cource))
		{
			JOptionPane.showMessageDialog(panel, "Alla lektioner m�ste vara minst " + SHORTEST_COURCE_LENGTH
					+ " minuter l�nga.");
		}
		
		else
		{			
			try
			{
				Controller.database.getEntityManager().persist(cource);
									
				Controller.database.commit();
						
				JOptionPane.showMessageDialog(null, "Lektionen sparades");
			}
			catch(Exception exp)
			{
				throw new ExitException();
			}
		}
		
		
	}
	/**
	 * Kontrollerar vissa v�rden f�r en ny lektion. Det kontrolleras att starttiden infaller f�re 
	 * sluttiden. Om inte s� �r fallet => return false.
	 * @return
	 */
	private boolean checkData()
	{
		if(((((NewCourceView) panel).getStartHour() * 60 + ((NewCourceView) panel).getStartMin()) -
				(((NewCourceView) panel).getEndHour() * 60 + ((NewCourceView) panel).getEndMin())
				>= 0))
		{
			return false;
		}
		
		
		return true;
	}
	
	
	/**
	 * F�r att kontrollera att det inte sedan tidigare finns en annan lektion p� den 
	 * dag och tid som anv�ndaren har angivit i formul�ret.
	 * @param cource
	 * @return
	 * @throws DatabaseException 
	 */
	private boolean courceOverlap(Cource cource) throws DatabaseException
	{
		Controller.database.createQuery("select x from Cource x");
		
		List<Model> result = new ArrayList<Model>();
				
		result = Controller.database.getResult(); 
		
		Iterator<Model> it = result.iterator();
		
		while(it.hasNext())
		{
			Cource earlierCource = (Cource) it.next(); // Den tidigare lektionen.
			
			if(!earlierCource.getDay().equals(cource.getDay())) // Om det inte �r samma dag => "continue".
			{
				continue;
			}
			
			// Om den nya lektionen startar f�re en tidigare lektion och samtidigt slutar senare �n den 
			// tidigare lektionen startar => lektionerna �verlappar => "return true".
			
			if(cource.courceStartedEarlier(earlierCource) && cource.getEndDayTime() > 
				earlierCource.getStartDayTime())
			{
				return true;
			}
			
			// Om den nya lektionen startar samtidigt (eller senare) som en annan lektion startar  
			// och den nya lektionen slutar tidigare (eller samtidigt) som den tidigare lektionen slutar	
			// => "return true"
			
			
			if(cource.getStartDayTime() >= earlierCource.getStartDayTime() &&
					cource.getEndDayTime() <= earlierCource.getEndDayTime())
			{
				return true;
			}
			
			// Om den nya lektionen startar samtidigt (eller senare) som en annan lektion startar  
			// och den nya lektionen slutar senare �n den tidigare lektionen => "return true".
			
			if(cource.getStartDayTime() >= earlierCource.getStartDayTime() &&
					!cource.courceEndedEarlier(earlierCource) && !(cource.getStartDayTime() >= 
					earlierCource.getEndDayTime()))
			{
				return true;
			}
			
			
			
			
		}
		
		
		return false;
	}
	
	/**
	 * Funktion f�r att kontrollera huruvida angiven lektion �r tillr�ckligt l�ng.
	 * @param cource	 
	 * @return
	 */		
	private boolean checkCourceLength(Cource cource)
	{
		if(getCourceLength(cource) >= SHORTEST_COURCE_LENGTH)
		{
			return true;
		}
		
		return false;
	}
	
	/**
	 * Funktion som r�knar ut l�ngden p� angiven lektion
	 * @param cource	
	 * @return
	 */
	private int getCourceLength(Cource cource)
	{
		return (cource.getEndHour() * 60 + cource.getEndMin()) - (cource.getStartHour() * 60 + 
				cource.getStartMin());
		
	}
	
	
	
	
	@Override
	public void actionPerformed(ActionEvent e)
	{
		
		
	}
	
	@Override
	public void itemStateChanged(ItemEvent e)
	{
		
		
	}
	
}
