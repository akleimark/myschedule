package controller;
/**
 * MySchedule is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MySchedule is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MySchedule. If not, see <http://www.gnu.org/licenses/>.
 */
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.util.List;
import javax.swing.JPanel;
import model.Model;
import view.NewCourceView;
import exception.DatabaseException;
import factory.HomeControllerFactory;
import factory.NewCourceViewFactory;

public class NewCourceController extends Controller
{
	
	
	public NewCourceController()
	{
		List<Model> result = null;
		
		Controller.viewFactory = new NewCourceViewFactory();
		Controller.panel = (JPanel) Controller.viewFactory.createProduct();
			
		
		try
		{
			Controller.database.createQuery("select x from SchoolSubject x");
			result = Controller.database.getResult();
			
		}
		catch (DatabaseException e1)
		{
			new DatabaseException();
		}
		
		((NewCourceView) panel).setDatabaseResults(result);
		
		((NewCourceView) panel).render();
		
		Controller.controllerFactory = new HomeControllerFactory();		
		
		try
		{
			((NewCourceView) panel).addController(Controller.controllerFactory.createProduct());
		}
		catch (DatabaseException e)
		{
			
			e.printStackTrace();
		} // Lyssnarna l�ggs till.		
		
	}
	
	
	
	
	
	
	@Override
	public void actionPerformed(ActionEvent e)
	{
		
	}
	
	@Override
	public void itemStateChanged(ItemEvent e)
	{
		
		
	}
	
}
