package factory;

/**
 * MySchedule is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MySchedule is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MySchedule. If not, see <http://www.gnu.org/licenses/>.
 */

import controller.Controller;
import exception.DatabaseException;

/**
 * Syftet med det h�r gr�nssnittet �r att l�ta en fabrik skapa de kontrollerare, 
 * som man �r intresserad av i st�llet f�r att skapa dem direkt. Om ett program beh�ver skapa m�nga objekt av 
 * olika typer �r detta ett perfekt m�nster att anv�nda. 
 * 
 * "Define an interface for creating an object, but let subclasses decide which class to instatiate. 
 * Factory Method lets a class defer instantiation to subclasses." - Gang of four.
 * 
 * @Utvecklare: Anders Kleimark
 *
 */

public interface ControllerFactory
{
	public abstract Controller createProduct() throws DatabaseException;
}
