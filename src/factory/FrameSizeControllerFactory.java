package factory;

import controller.Controller;
import controller.FrameSizeController;
import exception.DatabaseException;

public class FrameSizeControllerFactory implements ControllerFactory
{
	
	@Override
	public Controller createProduct() throws DatabaseException
	{
		return new FrameSizeController();
	}
	
}
