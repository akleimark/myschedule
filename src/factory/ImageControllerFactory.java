package factory;

/**
 * MySchedule is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MySchedule is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MySchedule. If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.FileNotFoundException;

import controller.Controller;
import controller.ImageController;
import exception.DatabaseException;

/**
* Den h�r fabriksklassen anv�nds f�r att skapa en "ImageController".
* @Utvecklare: Anders Kleimark
*
*/

public class ImageControllerFactory implements ControllerFactory
{
	/**
	 * Metod f�r att skapa en "ImageController".
	 */
	@Override
	public Controller createProduct() throws DatabaseException
	{
		ImageController imageController = null;
		
		try
		{
			imageController = new ImageController();
		}
		catch (FileNotFoundException e)
		{
			
			e.printStackTrace();
		}
		
		return imageController;
	}
	
}
