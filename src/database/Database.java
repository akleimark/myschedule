package database;

/**
 * MySchedule is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MySchedule is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MySchedule. If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.List;
import java.util.Vector;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import exception.DatabaseException;

import model.Model;
import model.SchoolSubject;

/**
 * Klassen Database anv�nds f�r att sl� upp saker i databasehanteraren. Vi anv�nder OpenJPA 
 * i den h�r applikationen. 
 * @Utvecklare: Anders Kleimark
 *
 */

public class Database
{
	private static final String PERSISTENCE_UNIT_NAME = "unit"; 
	private EntityManagerFactory factory; 
	private EntityManager em;	
	private EntityTransaction userTransaction;
	private Query query;
	private List<Model> results;
	
	/**
	 * F�rvald konstruktor som initierar klassens datamedlemmar och som ocks� startar OpenJPA. 
	 * 
	 */
	public Database()
	{
		
		factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		em = factory.createEntityManager();
		userTransaction = em.getTransaction();
				
		userTransaction.begin();
		
		try
		{
			init();
		}
		catch (DatabaseException e)
		{
			
			new DatabaseException();
		}							
				
	}
	
	/**
	 * Finns det ingen aktiv transaktion skapas en ny. 
	 */
	public void checkActivity()
	{
		if(!em.getTransaction().isActive())
		{
			em.getTransaction().begin();
		}
	}
	
	/**
	 * 
	 * @return: V�r "entityManager".
	 */
	public EntityManager getEntityManager()
	{
		return em;
	}
	
	/**
	 * �ppnar en ny transaktion.
	 */
	public void open()
	{
		factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		em = factory.createEntityManager();
		userTransaction = em.getTransaction();
				
		userTransaction.begin();
	}
	/**
	 * Initierar de tabeller som applikationen anv�nder. 
	 * @throws DatabaseException
	 */
	private void init() throws DatabaseException
	{
		/** Vi skapar de rader i databasen som handhar vissa grundl�ggande �mnen.
		*Exempel p� dessa �r: "matematik", "svenska", "historia", m.m.*/
		
		this.createQuery("select x from SchoolSubject x");
		this.getResult();
		
		
		if(size() == 0)
		{
			Vector<String> subjects = new Vector<String>();
			
			subjects.add("Matematik");
			subjects.add("Svenska");
			subjects.add("Engelska");
			subjects.add("Historia");
			subjects.add("Geografi");
			subjects.add("Religion");
			subjects.add("Samh�llskunskap");
			subjects.add("Fysik");
			subjects.add("Kemi");
			subjects.add("Biologi");
			subjects.add("Teknik");
			subjects.add("Datateknik");
			subjects.add("Idrott");
			subjects.add("Bild");
			subjects.add("Sl�jd");
			subjects.add("Rast");
			subjects.add("Lunch");
			subjects.add("Musik");
			
				
			
			// Databasen �r tom.
			
			for(int i = 0; i < subjects.size(); i++)
			{
				SchoolSubject subject = new SchoolSubject(subjects.get(i));
				em.persist(subject);			
			}
			
			em.getTransaction().commit();						
			
		}						
		
	}
	
	/**
	 * Skapar en SQL-str�ng.
	 * @param queryString
	 */
	public void createQuery(String queryString)
	{
		checkActivity();
		query = em.createQuery(queryString);				
	}
	
	//@SuppressWarnings("unchecked")
	@SuppressWarnings("unchecked")
	public List<Model> getResult() throws DatabaseException
	{
		
		results = (List<Model>) query.getResultList();
		
		return results;
						
	}
	
	/**
	 * K�r "persist" p� en angiven modell.
	 * @param model
	 */
	public void persist(Model model)
	{
		checkActivity();
		em.persist(model);		
		
	}
	
	/**
	 * Sparar.
	 */
	public void commit()
	{
		checkActivity();
		em.getTransaction().commit();
	}
	
	/**
	 * En standardutskrift.
	 */
	protected void print()
	{
		System.out.println("Utskrift");
	}

	/**
	 * Ger storleken p� den lista med data som har h�mtats. 
	 * @return
	 */
	public int size()
	{
		return results.size();
	}
		
}
